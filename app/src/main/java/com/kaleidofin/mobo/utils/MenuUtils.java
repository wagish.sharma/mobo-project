package com.kaleidofin.mobo.utils;

import android.view.View;

import androidx.fragment.app.FragmentActivity;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;
import com.kaleidofin.mobo.R;


public class MenuUtils {

    public static void hideNavigationMenu(FragmentActivity view) {
        // Hide menu
        BottomNavigationViewEx bottomNavigationViewEx = view.findViewById(R.id.bottomNavigationView);
        bottomNavigationViewEx.setVisibility(View.GONE);

    }

    public static void showNavigationMenu(FragmentActivity view) {
        // Hide menu
        BottomNavigationViewEx bottomNavigationViewEx = view.findViewById(R.id.bottomNavigationView);
        bottomNavigationViewEx.setVisibility(View.VISIBLE);

    }
}
