package com.kaleidofin.mobo.model.dto;

import java.math.BigDecimal;
import org.jetbrains.annotations.NotNull;

public class SpendingPattern {

    private Long id;
    private Integer version;
    private Long customerId;
    private String category;
    private BigDecimal minAmount;
    private BigDecimal maxAmount;
    private Long spendPercentile;
    private BigDecimal amount;
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public Integer getVersion() {
        return version;
    }
    public void setVersion(Integer version) {
        this.version = version;
    }
    public Long getCustomerId() {
        return customerId;
    }
    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }
    public String getCategory() {
        return category;
    }
    public void setCategory(String category) {
        this.category = category;
    }
    public BigDecimal getMinAmount() {
        return minAmount;
    }
    public void setMinAmount(BigDecimal minAmount) {
        this.minAmount = minAmount;
    }
    public BigDecimal getMaxAmount() {
        return maxAmount;
    }
    public void setMaxAmount(BigDecimal maxAmount) {
        this.maxAmount = maxAmount;
    }
    public Long getSpendPercentile() {
        return spendPercentile;
    }
    public void setSpendPercentile(Long spendPercentile) {
        this.spendPercentile = spendPercentile;
    }

    public SpendingPattern(Long id, Integer version, Long customerId, String category, BigDecimal minAmount, BigDecimal maxAmount, Long spendPercentile, BigDecimal amount) {
        this.id = id;
        this.version = version;
        this.customerId = customerId;
        this.category = category;
        this.minAmount = minAmount;
        this.maxAmount = maxAmount;
        this.spendPercentile = spendPercentile;
        this.amount = amount;
    }

    public SpendingPattern(Long id, Integer version, Long customerId, String category,
        BigDecimal minAmount, BigDecimal maxAmount, Long spendPercentile) {
        this.id = id;
        this.version = version;
        this.customerId = customerId;
        this.category = category;
        this.minAmount = minAmount;
        this.maxAmount = maxAmount;
        this.spendPercentile = spendPercentile;
    }

    public SpendingPattern() {
    }

    @NotNull
    @Override
    public String toString() {
        return "SpendingPatternCatagory{" +
            "id=" + id +
            ", version=" + version +
            ", customerId=" + customerId +
            ", category='" + category + '\'' +
            ", minAmount=" + minAmount +
            ", maxAmount=" + maxAmount +
            ", spendPercentile=" + spendPercentile +
            '}';
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
}
