package com.kaleidofin.mobo.model.dto;

import java.math.BigDecimal;

public class Goal {

    private Long id;
    private Integer version;
    private Long customerId;
    private String goalName;
    private BigDecimal goalAmount;
    private Long tenture;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public String getGoalName() {
        return goalName;
    }

    public void setGoalName(String goalName) {
        this.goalName = goalName;
    }

    public BigDecimal getGoalAmount() {
        return goalAmount;
    }

    public void setGoalAmount(BigDecimal goalAmount) {
        this.goalAmount = goalAmount;
    }

    public Long getTenture() {
        return tenture;
    }

    public void setTenture(Long tenture) {
        this.tenture = tenture;
    }
}
