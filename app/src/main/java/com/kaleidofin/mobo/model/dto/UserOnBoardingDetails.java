package com.kaleidofin.mobo.model.dto;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.UUID;

public class UserOnBoardingDetails {

    private String age;
    private String income;
    private String isHouseRented;
    private String isMarried;
    private String name;
    private String noOfChildren;

    public UserOnBoardingDetails(){

    }
    public UserOnBoardingDetails(String age, String income, String isHouseRented, String isMarried, String name, String noOfChildren) {
        this.age = age;
        this.income = income;
        this.isHouseRented = isHouseRented;
        this.isMarried = isMarried;
        this.name = name;
        this.noOfChildren = noOfChildren;
    }


    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getIncome() {
        return income;
    }

    public void setIncome(String income) {
        this.income = income;
    }

    public String getIsHouseRented() {
        return isHouseRented;
    }

    public void setIsHouseRented(String isHouseRented) {
        this.isHouseRented = isHouseRented;
    }

    public String getIsMarried() {
        return isMarried;
    }

    public void setIsMarried(String isMarried) {
        this.isMarried = isMarried;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNoOfChildren() {
        return noOfChildren;
    }

    public void setNoOfChildren(String numberOfChildren) {
        this.noOfChildren = numberOfChildren;
    }
}
