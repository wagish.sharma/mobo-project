package com.kaleidofin.mobo.network;

import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.Tasks;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GetTokenResult;
import java.io.IOException;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;
import org.jetbrains.annotations.NotNull;

public class FirebaseUserIdTokenInterceptor implements Interceptor {

    // Custom header for passing ID token in request.
    private static final String FIREBASE_SESSION_ID = "sessionId";

    @NotNull
    @Override
    public Response intercept(Chain chain) throws IOException {
        Request request = chain.request();

        try {
            FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
            if (user == null) {
                throw new Exception("User is not logged in.");
            } else {
                String sessionId = user.getUid();
                Request modifiedRequest = request.newBuilder()
                        .addHeader(FIREBASE_SESSION_ID, sessionId)
                        .build();
                return chain.proceed(modifiedRequest);
            }
        } catch (Exception e) {
            throw new IOException(e.getMessage());
        }
    }
}