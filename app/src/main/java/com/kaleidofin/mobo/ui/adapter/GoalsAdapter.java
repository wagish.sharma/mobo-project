package com.kaleidofin.mobo.ui.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;
import com.kaleidofin.mobo.R;
import com.kaleidofin.mobo.databinding.CustomerGoalsListItemBinding;
import com.kaleidofin.mobo.model.dto.Goal;
import com.kaleidofin.mobo.ui.adapter.GoalsAdapter.GoalsViewHolder;
import java.util.List;

public class GoalsAdapter extends RecyclerView.Adapter<GoalsViewHolder> {
  private List<Goal> goals;
  public GoalsAdapter() {
  }

  public void setCustomerGoals(
      List<Goal> goals) {
    this.goals = goals;
    notifyDataSetChanged();
  }

  @NonNull
  @Override
  public GoalsViewHolder onCreateViewHolder(@NonNull ViewGroup parent,
      int viewType) {
    CustomerGoalsListItemBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
        R.layout.customer_goals_list_item,parent,false);
    return new GoalsViewHolder(binding);
  }

  @Override
  public void onBindViewHolder(@NonNull GoalsViewHolder  holder, int position) {
    if(position != RecyclerView.NO_POSITION){
      holder.bind(goals.get(position));
    }
  }

  @Override
  public int getItemCount() {
    return goals == null? 0 : goals.size();
  }

  public static class GoalsViewHolder extends RecyclerView.ViewHolder{
    private CustomerGoalsListItemBinding binding;
    public GoalsViewHolder(CustomerGoalsListItemBinding binding) {
      super(binding.getRoot());
      this.binding = binding;
    }

    public void bind(Goal goal){
      this.binding.setGoal(goal);
    }
  }

}
