package com.kaleidofin.mobo.ui.dashboard;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.fragment.NavHostFragment;

import com.kaleidofin.mobo.R;
import com.kaleidofin.mobo.base.AutoClearedValue;
import com.kaleidofin.mobo.base.BaseFragment;
import com.kaleidofin.mobo.base.Resource;
import com.kaleidofin.mobo.databinding.FragmentDashboardBinding;
import com.kaleidofin.mobo.model.dto.SpendingPattern;
import com.kaleidofin.mobo.model.dto.SpendingPatternGroup;
import com.kaleidofin.mobo.ui.adapter.OverAllSpendingPatternCategoryAdapter;
import com.kaleidofin.mobo.utils.MenuUtils;
import com.kaleidofin.mobo.viewmodel.MoboApplicationViewModel;

import org.jetbrains.annotations.NotNull;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class DashboardFragment extends BaseFragment<MoboApplicationViewModel> {

    //position of shimmer layout in viewFlipper
    private static final int LOADING = 0;
    //position of content layout in viewFlipper
    private static final int CONTENT = 1;
    private static final String TAG = "DashboardFragment";
    private MoboApplicationViewModel moboApplicationViewModel;
    private final AutoClearedValue<FragmentDashboardBinding> bindingAutoClearedValue = new AutoClearedValue<>();
    private final AutoClearedValue<OverAllSpendingPatternCategoryAdapter> adapterAutoClearedValue = new AutoClearedValue<>();

    public DashboardFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        Log.d(TAG, "onCreateView: ");
        FragmentDashboardBinding binding = DataBindingUtil.inflate(inflater, R.layout.fragment_dashboard, container, false);
        bindingAutoClearedValue.setValue(this, binding);
        // show menu
        MenuUtils.showNavigationMenu(requireActivity());
        return binding.getRoot();
    }

    @Override
    public MoboApplicationViewModel getViewModel() {
        moboApplicationViewModel = new ViewModelProvider(requireActivity()).get(MoboApplicationViewModel.class);
        return moboApplicationViewModel;
    }

    @Override
    public int getNavHostFragmentId() {
        return R.id.nav_host_fragment;
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        moboApplicationViewModel.getOverallSpendingPatterns();
        bindingAutoClearedValue.get().setViewModel(moboApplicationViewModel);
        OverAllSpendingPatternCategoryAdapter adapter = new OverAllSpendingPatternCategoryAdapter();
        adapterAutoClearedValue.setValue(this, adapter);
        bindingAutoClearedValue.get().rvSpendingPatternOverall.setAdapter(adapter);
        moboApplicationViewModel.getSpendingPatternLiveData().observe(getViewLifecycleOwner(), this::renderViewState);
    }

    private void renderViewState(Resource<SpendingPatternGroup> resource) {
        if (resource != null) {
            switch (resource.status) {

                case LOADING:
                    bindingAutoClearedValue.get().setDisPlayedChild(LOADING);
                    bindingAutoClearedValue.get().includedLayout.shimmerProgressLayout.startShimmer();
                    break;
                case ERROR:
                    bindingAutoClearedValue.get().includedLayout.shimmerProgressLayout.stopShimmer();
                    bindingAutoClearedValue.get().setDisPlayedChild(CONTENT);
                    //TODO: For testing the UI rendering, remove 100 to 105 after checking
                    bindingAutoClearedValue.get().setTotalSpendModel(new SpendingPattern(12L,2,158L,"Total", BigDecimal.valueOf(4000),BigDecimal.valueOf(18000),70L,BigDecimal.valueOf(14500)));
                    List<SpendingPattern> spendingPatternCatagoryList = new ArrayList<>();
                    spendingPatternCatagoryList.add(new SpendingPattern(12L,2,158L,"Sports", BigDecimal.valueOf(100),BigDecimal.valueOf(2000),20L,BigDecimal.valueOf(1500)));
                    spendingPatternCatagoryList.add(new SpendingPattern(13L,3,158L,"Food", BigDecimal.valueOf(100),BigDecimal.valueOf(2000),70L,BigDecimal.valueOf(600)));
                    spendingPatternCatagoryList.add(new SpendingPattern(14L,4,158L,"Health", BigDecimal.valueOf(100),BigDecimal.valueOf(2000),90L,BigDecimal.valueOf(1000)));
                    adapterAutoClearedValue.get().setOverallSpendingPatternCatagories(spendingPatternCatagoryList);
                    break;
                case SUCCESS:
                    bindingAutoClearedValue.get().includedLayout.shimmerProgressLayout.stopShimmer();
                    bindingAutoClearedValue.get().setDisPlayedChild(CONTENT);
                    adapterAutoClearedValue.get().setOverallSpendingPatternCatagories(resource.viewState.getSpendingPatternList());
                    break;
            }

        }
    }
}
