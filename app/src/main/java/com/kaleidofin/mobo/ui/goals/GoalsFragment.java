package com.kaleidofin.mobo.ui.goals;

import android.os.Bundle;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.lifecycle.ViewModelProvider;
import com.kaleidofin.mobo.R;
import com.kaleidofin.mobo.base.AutoClearedValue;
import com.kaleidofin.mobo.base.BaseFragment;
import com.kaleidofin.mobo.databinding.FragmentGoalsBinding;
import com.kaleidofin.mobo.utils.MenuUtils;
import com.kaleidofin.mobo.viewmodel.MoboApplicationViewModel;

/**
 * A simple {@link Fragment} subclass.
 */
public class GoalsFragment extends BaseFragment<MoboApplicationViewModel> {
    private static final String TAG = "GoalsFragment";
    //position of shimmer layout in viewFlipper
    private static final int LOADING = 0;
    //position of content layout in viewFlipper
    private static final int CONTENT = 1;

    private MoboApplicationViewModel moboApplicationViewModel;
    private final AutoClearedValue<FragmentGoalsBinding> bindingAutoClearedValue = new AutoClearedValue<>();

    public GoalsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        Log.d(TAG, "onCreateView: ");
        FragmentGoalsBinding binding = DataBindingUtil.inflate(inflater, R.layout.fragment_goals, container, false);
        bindingAutoClearedValue.setValue(this, binding);
        // show menu
        MenuUtils.showNavigationMenu(requireActivity());
        return binding.getRoot();
    }

    @Override
    public MoboApplicationViewModel getViewModel() {
        moboApplicationViewModel = new ViewModelProvider(requireActivity()).get(MoboApplicationViewModel.class);
        return moboApplicationViewModel;
    }

    @Override
    public int getNavHostFragmentId() {
        return R.id.nav_host_fragment;
    }
}
