package com.kaleidofin.mobo.ui.goals;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.fragment.NavHostFragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.kaleidofin.mobo.R;

import butterknife.BindView;
import butterknife.ButterKnife;


public class GoalsIntroFragment extends Fragment {
    @BindView(R.id.btn_consent_screen)
    Button btnConsentScreen;
    NavController navController;
    public GoalsIntroFragment() {
        // Required empty public constructor
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_goals_intro, container, false);
        ButterKnife.bind(this,view);
        navController = NavHostFragment.findNavController(this);
        btnConsentScreen.setOnClickListener(v->navController.navigate(R.id.action_goalsIntroFragment_to_consentFragment));
        return view;

    }


}
