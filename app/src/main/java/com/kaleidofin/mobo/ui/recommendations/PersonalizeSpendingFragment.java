package com.kaleidofin.mobo.ui.recommendations;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.fragment.NavHostFragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.kaleidofin.mobo.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import com.kaleidofin.mobo.base.AutoClearedValue;
import com.kaleidofin.mobo.base.BaseFragment;
import com.kaleidofin.mobo.base.Resource;
import com.kaleidofin.mobo.databinding.FragmentPersonalizeSpendingBinding;
import com.kaleidofin.mobo.databinding.FragmentSpendingRecommendationBinding;
import com.kaleidofin.mobo.model.dto.SpendingPatternGroup;
import com.kaleidofin.mobo.ui.adapter.PersonalizedSpendingPatternAdapter;
import com.kaleidofin.mobo.viewmodel.MoboApplicationViewModel;
import org.jetbrains.annotations.NotNull;


public class PersonalizeSpendingFragment extends BaseFragment<MoboApplicationViewModel> {
    private NavController navController;
    private MoboApplicationViewModel applicationViewModel;
    private final AutoClearedValue<FragmentPersonalizeSpendingBinding> bindingAutoClearedValue = new AutoClearedValue<>();
    public PersonalizeSpendingFragment() {
        // Required empty public constructor
    }


    @Override
    public MoboApplicationViewModel getViewModel() {
        applicationViewModel = new ViewModelProvider(requireActivity()).get(MoboApplicationViewModel.class);
        return applicationViewModel;
    }

    @Override
    public int getNavHostFragmentId() {
        return R.id.nav_host_fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        FragmentPersonalizeSpendingBinding binding = DataBindingUtil.inflate(inflater,R.layout.fragment_personalize_spending, container, false);
        bindingAutoClearedValue.setValue(this,binding);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        navController = NavHostFragment.findNavController(this);

//        applicationViewModel.getSpendingPatternLiveData().observe(getViewLifecycleOwner(),this::renderViewState);
        bindingAutoClearedValue.get().btnGoalIntro.setOnClickListener(v -> navController.navigate(R.id.action_personalizeSpendingFragment_to_goalsIntroFragment));
    }


//    private void renderViewState(Resource<SpendingPatternGroup> resource){
//        switch (resource.status){
//            case SUCCESS:
//                PersonalizedSpendingPatternAdapter adapter = new PersonalizedSpendingPatternAdapter(resource.viewState.getTotalSpendingPattern());
//                adapter.setSpendingPatternCatagories(resource.viewState.getSpendingPatternList());
//                break;
//        }
//
//    }
}
