package com.kaleidofin.mobo.ui.auth;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.google.firebase.auth.PhoneAuthProvider;
import com.kaleidofin.mobo.R;
import com.kaleidofin.mobo.auth.CountryData;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RegisterFragment extends Fragment {

    @BindView(R.id.editTextPhone)
    EditText editText;

    @BindView(R.id.spinnerCountries)
    Spinner spinner;

    @BindView(R.id.buttonContinueOTP)
    Button continueToOTP;
    private static final String TAG = "RegisterFragment";
    public RegisterFragment() {
        // Required empty public constructor
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_register, container, false);
        ButterKnife.bind(this, view);
        spinner.setAdapter(new ArrayAdapter<String>(requireActivity(), android.R.layout.simple_spinner_dropdown_item, CountryData.countryNames));
        ArrayAdapter<String> spinnerAdap = (ArrayAdapter<String>) spinner.getAdapter();
        int spinnerPosition = spinnerAdap.getPosition("India");
        spinner.setSelection(spinnerPosition);

        continueToOTP.setOnClickListener(v -> {
            String code = CountryData.countryAreaCodes[spinner.getSelectedItemPosition()];

            Log.d(TAG, "onCreateView: code"+code);
            String number = editText.getText().toString().trim();

            if (number.isEmpty() || number.length() < 10) {
                editText.setError("Valid number is required");
                editText.requestFocus();
                return;
            }



            goToOtpScreen(code, number);

        });
        return view;
    }

    void goToOtpScreen(String countryCode,String phoneNumber){
        RegisterFragmentDirections.ActionRegisterFragmentToOtpFragment action = RegisterFragmentDirections.actionRegisterFragmentToOtpFragment(phoneNumber,countryCode);
        Navigation.findNavController(continueToOTP).navigate(action);
    }
}
