package com.kaleidofin.mobo.ui;

import androidx.appcompat.app.AppCompatActivity;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.NavigationUI;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;
import com.kaleidofin.mobo.R;
import com.tyagiabhinav.dialogflowchatlibrary.Chatbot;
import com.tyagiabhinav.dialogflowchatlibrary.ChatbotActivity;
import com.tyagiabhinav.dialogflowchatlibrary.ChatbotSettings;
import com.tyagiabhinav.dialogflowchatlibrary.DialogflowCredentials;

import java.util.UUID;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "MainActivity";
    private NavController currentNav;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.d(TAG, "onCreate: "+(savedInstanceState==null));
        if(savedInstanceState == null) {
            createBottomNavigation();
        }

    }
    /**
     * Create bottom menu
     */
    public void createBottomNavigation() {
        Log.d(TAG, "createBottomNavigation: here");
        BottomNavigationViewEx bottomNavigationView =  findViewById(R.id.bottomNavigationView);
        // Setup the bottom navigation view with a list of navigation graphs

        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupWithNavController(bottomNavigationView, navController);
        currentNav = navController;
        bottomNavigationView.setOnNavigationItemSelectedListener(menuItem -> {
            int menuId = menuItem.getItemId();
            if(menuId ==  R.id.chatFragment){
                DialogflowCredentials.getInstance().setInputStream(getResources().openRawResource(R.raw.banking_bot));
                ChatbotSettings.getInstance().setChatbot( new Chatbot.ChatbotBuilder()
//                .setDoAutoWelcome(false) // True by Default, False if you do not want the Bot to greet the user Automatically. Dialogflow agent must have a welcome intent to handle this
//                .setChatBotAvatar(getDrawable(R.drawable.avatarBot)) // provide avatar for your bot if default is not required
//                .setChatUserAvatar(getDrawable(R.drawable.avatarUser)) // provide avatar for your the user if default is not required
                        .setShowMic(true) // False by Default, True if you want to use Voice input from the user to chat
                        .build());

                Bundle bundle = new Bundle();

                // provide a UUID for your session with the Dialogflow agent
                bundle.putString(ChatbotActivity.SESSION_ID, UUID.randomUUID().toString());

                Intent intent = new Intent(MainActivity.this,ChatbotActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_NO_HISTORY);
                intent.putExtras(bundle);
                startActivity(intent);
                return false;
            }else{
                NavigationUI.onNavDestinationSelected(menuItem, navController);
                return true;
            }
        });


    }
}
