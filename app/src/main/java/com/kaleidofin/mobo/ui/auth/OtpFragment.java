package com.kaleidofin.mobo.ui.auth;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskExecutors;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.kaleidofin.mobo.R;

import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;


public class OtpFragment extends Fragment {
    private String countryCode;
    private String phoneNumber;

    private String verificationid;
    private FirebaseAuth mAuth;
    @BindView(R.id.progressbar)
    ProgressBar progressBar;
    @BindView(R.id.editTextCode)
    EditText editText;
    @BindView(R.id.buttonSignIn)
    Button buttonSignIn;
    @BindView(R.id.mobile_number)
    TextView otpSentTo;
    private static final String TAG = "OtpFragment";

    public OtpFragment() {
        // Required empty public constructor
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_otp, container, false);
        mAuth = FirebaseAuth.getInstance();
        ButterKnife.bind(this,view);

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if(getArguments() != null){
            countryCode = OtpFragmentArgs.fromBundle(getArguments()).getCountyCode();
            Log.d(TAG, "onViewCreated: countryCode"+countryCode);

            phoneNumber = OtpFragmentArgs.fromBundle(getArguments()).getPhoneNumber();
            String phoneNumberWithCountryCode = "+" + countryCode + phoneNumber;
            Log.d(TAG, "onViewCreated: phoneNumberWithCountryCode"+phoneNumberWithCountryCode);
            sendVerificationCode(phoneNumberWithCountryCode);

            buttonSignIn.setOnClickListener(v -> {

                String code = editText.getText().toString().trim();

                if ((code.isEmpty() || code.length() < 6)){

                    editText.setError("Enter code...");
                    editText.requestFocus();
                    return;
                }
                verifyCode(code);

            });
        }
    }


    private void verifyCode(String code){
        PhoneAuthCredential credential = PhoneAuthProvider.getCredential(verificationid, code);
        signInWithCredential(credential);
    }

    private void signInWithCredential(PhoneAuthCredential credential) {
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {

                            //Intent intent = new Intent(OtpActivity.this, MainActivity.class);
                            //intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

                            //startActivity(intent);
                            Log.d(TAG, "onComplete: signin successs");
                            Navigation.findNavController(buttonSignIn).navigate(OtpFragmentDirections.actionOtpFragmentToChatIntroFragment());
                        } else {
                            Toast.makeText(requireActivity(), task.getException().getMessage(), Toast.LENGTH_LONG).show();
                        }
                    }

                });
    }
    private void sendVerificationCode(String number){

        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                number,
                60,
                TimeUnit.SECONDS,
                TaskExecutors.MAIN_THREAD,
                mCallBack
        );
        otpSentTo.setText("OTP sent to "+number);
    }

    private PhoneAuthProvider.OnVerificationStateChangedCallbacks
            mCallBack = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {

        @Override
        public void onCodeSent(String s, PhoneAuthProvider.ForceResendingToken forceResendingToken) {
            super.onCodeSent(s, forceResendingToken);
            verificationid = s;
        }


        @Override
        public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {
            String code = phoneAuthCredential.getSmsCode();
            Log.d(TAG, "onVerificationCompleted: "+code);
            if (code != null){
                progressBar.setVisibility(View.VISIBLE);
                verifyCode(code);
            }
        }

        @Override
        public void onVerificationFailed(FirebaseException e) {
            Toast.makeText(requireActivity(), e.getMessage(),Toast.LENGTH_LONG).show();

        }
    };

}
