package com.kaleidofin.mobo.ui.recommendations;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.fragment.NavHostFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.kaleidofin.mobo.R;
import com.kaleidofin.mobo.base.AutoClearedValue;
import com.kaleidofin.mobo.base.BaseFragment;
import com.kaleidofin.mobo.base.Resource;
import com.kaleidofin.mobo.databinding.FragmentSpendingRecommendationBinding;
import com.kaleidofin.mobo.model.dto.SpendingPattern;
import com.kaleidofin.mobo.model.dto.SpendingPatternGroup;
import com.kaleidofin.mobo.ui.adapter.SpendingPatternCategoryAdapter;
import com.kaleidofin.mobo.viewmodel.MoboApplicationViewModel;
import com.tyagiabhinav.dialogflowchatlibrary.Chatbot;
import com.tyagiabhinav.dialogflowchatlibrary.ChatbotActivity;
import com.tyagiabhinav.dialogflowchatlibrary.ChatbotSettings;
import com.tyagiabhinav.dialogflowchatlibrary.DialogflowCredentials;

import java.util.List;
import java.util.UUID;

import org.jetbrains.annotations.NotNull;


public class SpendingRecommendationFragment extends BaseFragment<MoboApplicationViewModel> {
    //position of shimmer layout in viewFlipper
    private static final int LOADING = 0;
    //position of content layout in viewFlipper
    private static final int CONTENT = 1;
    private NavController navController;
    private final AutoClearedValue<FragmentSpendingRecommendationBinding> bindingAutoClearedValue = new AutoClearedValue<>();
    private final AutoClearedValue<SpendingPatternCategoryAdapter> adapterAutoClearedValue = new AutoClearedValue<>();
    private MoboApplicationViewModel moboApplicationViewModel;
    public SpendingRecommendationFragment() {
        // Required empty public constructor
    }



    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        FragmentSpendingRecommendationBinding binding  = DataBindingUtil.inflate(inflater, R.layout.fragment_spending_recommendation, container, false);
        bindingAutoClearedValue.setValue(this,binding);
        return binding.getRoot();
    }

    @Override
    public MoboApplicationViewModel getViewModel() {
        moboApplicationViewModel = new ViewModelProvider(requireActivity()).get(MoboApplicationViewModel.class);
        return moboApplicationViewModel;
    }

    @Override
    public int getNavHostFragmentId() {
        return R.id.nav_host_fragment;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        navController = NavHostFragment.findNavController(this);
        moboApplicationViewModel.getDefaultSpendingPatterns();
        int[] colorArray = getResources().getIntArray(R.array.progress_colors);
        SpendingPatternCategoryAdapter adapter = new SpendingPatternCategoryAdapter(colorArray);
        adapterAutoClearedValue.setValue(this,adapter);
        bindingAutoClearedValue.get().spendingCategoryRecyclerView.setAdapter(adapter);
        moboApplicationViewModel.getSpendingPatternLiveData().observe(getViewLifecycleOwner(),this::renderViewState);
        bindingAutoClearedValue.get().btnPersonalizeSpending.setOnClickListener(v -> navController.navigate(R.id.action_spendingRecommendationFragment_to_personalizeSpendingFragment));
//        startChat();
    }


    private void renderViewState(Resource<SpendingPatternGroup> resource){
        if(resource != null){
            switch (resource.status){

                case LOADING:
                    bindingAutoClearedValue.get().setDisPlayedChild(LOADING);
                    bindingAutoClearedValue.get().includedLayout.shimmerProgressLayout.startShimmer();
                    break;
                case ERROR:
                    bindingAutoClearedValue.get().includedLayout.shimmerProgressLayout.stopShimmer();
                    bindingAutoClearedValue.get().setDisPlayedChild(CONTENT);
                    break;
                case SUCCESS:
                    bindingAutoClearedValue.get().includedLayout.shimmerProgressLayout.stopShimmer();
                    bindingAutoClearedValue.get().setDisPlayedChild(CONTENT);
                    adapterAutoClearedValue.get().setSpendingPatternCatagories(resource.viewState.getSpendingPatternList());
                    break;
            }

        }
    }

}
