package com.kaleidofin.mobo.ui.consent;

import static com.kaleidofin.mobo.utils.Constants.CLIENT_ID;
import static com.kaleidofin.mobo.utils.Constants.ORGANIZATION_ID;
import static com.kaleidofin.mobo.utils.Constants.PRODUCT_ID;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.finpro.sdk.FinPro;
import com.finpro.sdk.FinProType;
import com.finpro.sdk.PartyIdentifierType;
import com.google.firebase.auth.FirebaseAuth;
import com.kaleidofin.mobo.R;
import com.kaleidofin.mobo.utils.Constants;
import com.kaleidofin.mobo.viewmodel.ConsentViewModel;

/**
 * A simple {@link Fragment} subclass.
 */
public class ConsentFragment extends Fragment {

    private static final String TAG = "ConsentFragment";

    String accountId = "";// dynamic
    String phoenNumber = "";// dynamic
    private boolean launchedSDK = false;
    private ConsentViewModel consentViewModel;
    public ConsentFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        Log.d(TAG, "onCreate: ");
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onAttachFragment(@NonNull Fragment childFragment) {
        Log.d(TAG, "onAttachFragment: ");
        super.onAttachFragment(childFragment);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        Log.d(TAG, "onCreateView: ");
        View view =  inflater.inflate(R.layout.fragment_consent, container, false);
        consentViewModel =  new ViewModelProvider(this).get(ConsentViewModel.class);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Log.d(TAG, "onViewCreated: "+consentViewModel.getIsConsentRequested() );
        if(!consentViewModel.getIsConsentRequested()){
            accountId = FirebaseAuth.getInstance().getCurrentUser().getUid();
            phoenNumber = FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber().substring(3);

            consentViewModel.setIsConsentRequested(true);

            requestConsent();
        }else{
            Navigation.findNavController(view).navigate(R.id.action_consentFragment_to_dashboardFragment);
        }


    }

    public void requestConsent(){
        Log.d(TAG, "requestConsent: accountid:"+accountId);
        FinPro.init(requireActivity(), ORGANIZATION_ID,CLIENT_ID)
                .setParams(PartyIdentifierType.MOBILE,phoenNumber,PRODUCT_ID,accountId)
                .setFinProType(FinProType.SELECT_AGREGGATOR).launch();
    }
    public void showConsent(){
        Log.d(TAG, "showConsent: ");
        FinPro.init(requireActivity(),ORGANIZATION_ID,CLIENT_ID)
                .setParams(PartyIdentifierType.MOBILE,phoenNumber,PRODUCT_ID,accountId)
                .setFinProType(FinProType.CONSENTS_LIST).launch();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d(TAG, "onActivityResult: requestCode"+requestCode);
        Log.d(TAG, "onActivityResult: resultCode"+resultCode);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.d(TAG, "onActivityCreated: ");
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG, "onResume: ");
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.d(TAG, "onStart: ");
        if(consentViewModel.getIsConsentRequested()){
           //TODO move to dashboard fragment
            View view = getActivity().findViewById(R.id.nav_host_fragment);
            Navigation.findNavController(view).navigate(R.id.action_consentFragment_to_dashboardFragment);
            consentViewModel.setIsConsentRequested(false);

        }
    }
}
