package com.kaleidofin.mobo.ui.goals;

import android.os.Bundle;

import android.util.Log;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.lifecycle.ViewModelProvider;
import com.kaleidofin.mobo.R;
import com.kaleidofin.mobo.base.AutoClearedValue;
import com.kaleidofin.mobo.base.BaseFragment;
import com.kaleidofin.mobo.databinding.FragmentCreateGoalBinding;
import com.kaleidofin.mobo.utils.MenuUtils;
import com.kaleidofin.mobo.viewmodel.MoboApplicationViewModel;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Arrays;
import java.util.List;
import org.jetbrains.annotations.NotNull;


public class CreateGoalFragment extends BaseFragment<MoboApplicationViewModel> {
    private final AutoClearedValue<FragmentCreateGoalBinding> bindingAutoClearedValue = new AutoClearedValue<>();
    private MoboApplicationViewModel applicationViewModel;
    private List<Integer> tenureList = Arrays.asList(6,12, 24);
    private List<Integer> amountList = Arrays.asList(100000, 200000, 300000);
    private int selectedTenure = tenureList.get(1);
    private long selectedAmount = amountList.get(1);
    private String goalName;
    public CreateGoalFragment() {
        // Required empty public constructor
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        FragmentCreateGoalBinding binding = DataBindingUtil.inflate(inflater,R.layout.fragment_create_goal, container, false);
        bindingAutoClearedValue.setValue(this,binding);
        binding.goalAmountTextView.setText(getString(R.string.money_placeholder,String.valueOf(selectedAmount)));
        binding.tenureTextView.setText(getTenure(selectedTenure));
        return binding.getRoot();
    }


    private String getTenure(int months){
        if(months < 12) {
            return months + " months";
        } else{
            int year = months / 12;
            if(year > 1){
                return year + " years";
            }
            return   year +" year";
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        MenuUtils.hideNavigationMenu(requireActivity());
        if(getArguments() != null){
            goalName = CreateGoalFragmentArgs.fromBundle(getArguments()).getGoalName();
            bindingAutoClearedValue.get().goalName.setText(goalName);
        }
        bindingAutoClearedValue.get().radioGroup.setOnCheckedChangeListener(
            (group, checkedId) -> {
                selectedAmount = getAmount(checkedId);
                bindingAutoClearedValue.get().goalAmountTextView.setText(getString(R.string.money_placeholder,String.valueOf(selectedAmount)));
            });
        bindingAutoClearedValue.get().tenureRadioGroup.setOnCheckedChangeListener(
            (group, checkedId) -> {
                selectedTenure = getTenureFromId(checkedId);
                bindingAutoClearedValue.get().tenureTextView.setText(getTenure(selectedTenure));
            });
        bindingAutoClearedValue.get().button.setOnClickListener((view1) -> {
            applicationViewModel.navigateAndCalculateContributionAmount(goalName,selectedAmount,selectedTenure);
        });
    }

    private int getTenureFromId(int id){
        switch (id){
            case R.id.tenure_one:
                return tenureList.get(0);
            case R.id.tenure_two:
                return tenureList.get(1);
            case R.id.tenure_three:
                return tenureList.get(2);
            default:
                return tenureList.get(1);
        }

    }


    private  int getAmount(int id){
        switch (id){
            case R.id.amount_one:
                return amountList.get(0);
            case R.id.amount_two:
                return amountList.get(1);
            case R.id.amount_three:
                return amountList.get(2);
            default:
                return amountList.get(1);
        }
    }

    @Override
    public MoboApplicationViewModel getViewModel() {
        applicationViewModel = new ViewModelProvider(requireActivity()).get(MoboApplicationViewModel.class);
        return applicationViewModel;
    }




    @Override
    public int getNavHostFragmentId() {
        return R.id.nav_host_fragment;
    }
}
