package com.kaleidofin.mobo.ui.goals;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import com.kaleidofin.mobo.R;
import com.kaleidofin.mobo.base.AutoClearedValue;
import com.kaleidofin.mobo.base.BaseFragment;
import com.kaleidofin.mobo.base.Resource;
import com.kaleidofin.mobo.databinding.FragmentConfirmGoalBinding;
import com.kaleidofin.mobo.model.dto.Goal;
import com.kaleidofin.mobo.model.dto.SpendingPatternGroup;
import com.kaleidofin.mobo.viewmodel.MoboApplicationViewModel;
import java.math.BigDecimal;

/**
 * A simple {@link Fragment} subclass.
 */
public class ConfirmGoalFragment extends BaseFragment<MoboApplicationViewModel> {
    //position of shimmer layout in viewFlipper
    private static final int LOADING = 0;
    //position of content layout in viewFlipper
    private static final int CONTENT = 1;
    private MoboApplicationViewModel applicationViewModel;
    private final AutoClearedValue<FragmentConfirmGoalBinding> bindingAutoClearedValue = new AutoClearedValue<>();
    public ConfirmGoalFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
         FragmentConfirmGoalBinding binding = DataBindingUtil.inflate(inflater,R.layout.fragment_confirm_goal, container, false);
         bindingAutoClearedValue.setValue(this,binding);
         return binding.getRoot();
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        bindingAutoClearedValue.get().setDisPlayedChild(CONTENT);
        setUpToolBarWithNavigationUI(bindingAutoClearedValue.get().toolbar);
        applicationViewModel.getContributionAmount().observe(getViewLifecycleOwner(),
            contributionAmount -> bindingAutoClearedValue.get().contributionTextView.setText(getString(R.string.money_placeholder,String.valueOf(contributionAmount))));
        bindingAutoClearedValue.get().button.setOnClickListener((view1)->applicationViewModel.createGoal());
        applicationViewModel.getGoalLiveData().observe(getViewLifecycleOwner(),this::renderViewState);
    }

    private void renderViewState(Resource<Goal> resource){
        if(resource != null){
            switch (resource.status){
                case LOADING:
                    bindingAutoClearedValue.get().setDisPlayedChild(LOADING);
                    bindingAutoClearedValue.get().includedLayout.shimmerProgressLayout.startShimmer();
                    break;
                case ERROR:
                case SUCCESS:
                    bindingAutoClearedValue.get().includedLayout.shimmerProgressLayout.stopShimmer();
                    bindingAutoClearedValue.get().setDisPlayedChild(CONTENT);
                    break;
            }

        }
    }


    @Override
    public MoboApplicationViewModel getViewModel() {
        applicationViewModel = new ViewModelProvider(requireActivity()).get(MoboApplicationViewModel.class);
        return applicationViewModel;
    }

    @Override
    public int getNavHostFragmentId() {
        return R.id.nav_host_fragment;
    }
}
