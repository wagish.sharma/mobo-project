package com.kaleidofin.mobo.ui.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;
import androidx.annotation.ColorInt;
import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;
import com.kaleidofin.mobo.R;
import com.kaleidofin.mobo.databinding.PersonalizedSpendingListItemBinding;
import com.kaleidofin.mobo.model.dto.SpendingPattern;
import java.util.List;

public class PersonalizedSpendingPatternAdapter extends RecyclerView.Adapter<PersonalizedSpendingPatternAdapter.SpendingPatternCategoryViewHolder> {
  private List<SpendingPattern> spendingPatternCatagories;
  private SpendingPattern totalSpendingPattern;
  public PersonalizedSpendingPatternAdapter(SpendingPattern totalSpendingPattern) {
    this.totalSpendingPattern = totalSpendingPattern;
  }

  public void setSpendingPatternCatagories(
      List<SpendingPattern> spendingPatternCatagories) {
    this.spendingPatternCatagories = spendingPatternCatagories;
    notifyDataSetChanged();
  }


  @NonNull
  @Override
  public SpendingPatternCategoryViewHolder onCreateViewHolder(@NonNull ViewGroup parent,
      int viewType) {
    PersonalizedSpendingListItemBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
        R.layout.personalized_spending_list_item,parent,false);
    return new SpendingPatternCategoryViewHolder(binding,totalSpendingPattern);
  }

  @Override
  public void onBindViewHolder(@NonNull SpendingPatternCategoryViewHolder holder, int position) {
    if(position != RecyclerView.NO_POSITION){
      holder.bind(spendingPatternCatagories.get(position));
    }
  }

  @Override
  public int getItemCount() {
    return spendingPatternCatagories == null? 0 : spendingPatternCatagories.size();
  }

  public static class SpendingPatternCategoryViewHolder extends RecyclerView.ViewHolder{
    private PersonalizedSpendingListItemBinding binding;
    private SpendingPattern totalSpendingPattern;
    public SpendingPatternCategoryViewHolder(PersonalizedSpendingListItemBinding binding, SpendingPattern totalSpendingPattern) {
      super(binding.getRoot());
      this.binding = binding;
      this.totalSpendingPattern = totalSpendingPattern;

    }

    public void bind(SpendingPattern spendingPattern){
      this.binding.setDefaultAmount(spendingPattern.getAmount());
      this.binding.setSpendingCategoryTitle(spendingPattern.getCategory());
      this.binding.setSpentPercentage(spendingPattern.getSpendPercentile());
      this.binding.setAmount(spendingPattern.getAmount());

    }
  }
}





