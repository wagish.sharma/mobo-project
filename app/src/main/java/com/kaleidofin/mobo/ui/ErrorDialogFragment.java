package com.kaleidofin.mobo.ui;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;
import com.kaleidofin.mobo.utils.DialogButtonActions;

public class ErrorDialogFragment extends DialogFragment {
    private AppCompatActivity activityContext;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        try {
            this.activityContext = (AppCompatActivity) context;
        } catch (ClassCastException exception) {
            throw new IllegalArgumentException("The parent Activity must extend AppCompatActivity");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        activityContext = null;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        String title = null;
        String message = null;
        String positiveButtonText = null;
        String negativeButtonText = null;
        DialogButtonActions buttonActions;
        DialogInterface.OnClickListener positiveButtonAction = null;
        DialogInterface.OnClickListener negativeButtonAction = null;
        boolean isCancelable = true;
        if (getArguments() != null) {
            title = ErrorDialogFragmentArgs.fromBundle(getArguments()).getDialogTitle();
            message = ErrorDialogFragmentArgs.fromBundle(getArguments()).getDialogMessage();
            buttonActions = ErrorDialogFragmentArgs.fromBundle(getArguments()).getButtonActions();
            isCancelable = ErrorDialogFragmentArgs.fromBundle(getArguments()).getIsDialogCancelable();
            if(buttonActions != null){
                positiveButtonAction = buttonActions.getPositiveAction();
                negativeButtonAction = buttonActions.getNegativeAction();
            }
            positiveButtonText = ErrorDialogFragmentArgs.fromBundle(getArguments()).getPositionButtonText();
            negativeButtonText = ErrorDialogFragmentArgs.fromBundle(getArguments()).getNegativeButtonText();
        }
        AlertDialog.Builder dialogBuilder = new Builder(activityContext);
        setCancelable(isCancelable);
        if (title != null) {
            dialogBuilder.setTitle(title);
        }
        if (message != null) {
            dialogBuilder.setMessage(message);
        } else {
            dialogBuilder.setMessage("Something went wrong");
        }
        if (positiveButtonAction != null && positiveButtonText != null) {
            dialogBuilder.setPositiveButton(positiveButtonText, positiveButtonAction);
        }
        if (negativeButtonAction != null && negativeButtonText != null) {
            dialogBuilder.setNegativeButton(negativeButtonText, negativeButtonAction);
        }

        return dialogBuilder.create();
    }
}