package com.kaleidofin.mobo.ui.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.ColorInt;
import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.kaleidofin.mobo.R;
import com.kaleidofin.mobo.databinding.OverallSpendingPatternListItemBinding;
import com.kaleidofin.mobo.model.dto.SpendingPattern;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class OverAllSpendingPatternCategoryAdapter extends RecyclerView.Adapter<OverAllSpendingPatternCategoryAdapter.SpendingPatternCategoryViewHolder> {

    private List<SpendingPattern> spendingPatternCategories = new ArrayList<>();
    private int[] colorArray;
    private int[] backColorArray;

    public OverAllSpendingPatternCategoryAdapter() {
    }

    public void setOverallSpendingPatternCatagories(
            List<SpendingPattern> spendingPatternCatagories) {
        this.spendingPatternCategories = spendingPatternCatagories;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public SpendingPatternCategoryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        OverallSpendingPatternListItemBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.overall_spending_pattern_list_item, parent, false);
        return new SpendingPatternCategoryViewHolder(binding, spendingPatternCategories);
    }

    @Override
    public void onBindViewHolder(@NonNull SpendingPatternCategoryViewHolder holder, int position) {
        if (position != RecyclerView.NO_POSITION) {
            this.colorArray = holder.itemView.getResources().getIntArray(R.array.spend_pattern_colors);
            this.backColorArray = holder.itemView.getResources().getIntArray(R.array.spend_pattern_back_colors);
            holder.bind(spendingPatternCategories.get(position), colorArray[position], backColorArray[position]);
        }
    }

    @Override
    public int getItemCount() {
        return spendingPatternCategories == null ? 1 : spendingPatternCategories.size();
    }

    public static class SpendingPatternCategoryViewHolder extends RecyclerView.ViewHolder {
        private OverallSpendingPatternListItemBinding binding;

        public SpendingPatternCategoryViewHolder(OverallSpendingPatternListItemBinding binding, List<SpendingPattern> spendingPatternCatagories) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void bind(SpendingPattern spendingPatternCatagory, @ColorInt int colorId, @ColorInt int color) {
            BigDecimal expenses = spendingPatternCatagory.getAmount().multiply(BigDecimal.valueOf(spendingPatternCatagory.getSpendPercentile())).divide(BigDecimal.valueOf(100));
            this.binding.setSpendingCategoryAmount(expenses);
            this.binding.setSpendingCategoryTitle(spendingPatternCatagory.getCategory());
            this.binding.setLeftAmount(spendingPatternCatagory.getAmount().subtract(expenses));
            this.binding.setColor(colorId);
            this.binding.setColorBack(color);
        }

    }


}
