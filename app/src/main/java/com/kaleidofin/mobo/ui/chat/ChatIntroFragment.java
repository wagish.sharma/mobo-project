package com.kaleidofin.mobo.ui.chat;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.fragment.NavHostFragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.kaleidofin.mobo.R;
import com.kaleidofin.mobo.utils.MenuUtils;
import com.tyagiabhinav.dialogflowchatlibrary.Chatbot;
import com.tyagiabhinav.dialogflowchatlibrary.ChatbotActivity;
import com.tyagiabhinav.dialogflowchatlibrary.ChatbotSettings;
import com.tyagiabhinav.dialogflowchatlibrary.DialogflowCredentials;

import java.util.UUID;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class ChatIntroFragment extends Fragment {

    @BindView(R.id.btn_continue_chat)
    Button continueToChat;

    private NavController navController;
    public ChatIntroFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_chat_intro, container, false);

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this,view);
        //startChat();
        //action_chatIntroFragment_to_chatFragment

        // configure navController
        navController = NavHostFragment.findNavController(this);

        // hide navigation menu
        MenuUtils.hideNavigationMenu(getActivity());

        // actions
        //continueToChat.setOnClickListener(v -> navController.navigate(R.id.action_chatIntroFragment_to_dashboardFragment));
        continueToChat.setOnClickListener(v -> navController.navigate(R.id.action_chatIntroFragment_to_confirmOnboardingDetailsFragment));

    }


    public void startChat(){
        DialogflowCredentials.getInstance().setInputStream(getResources().openRawResource(R.raw.mobo_onboarding));


        ChatbotSettings.getInstance().setChatbot( new Chatbot.ChatbotBuilder()
//                .setDoAutoWelcome(false) // True by Default, False if you do not want the Bot to greet the user Automatically. Dialogflow agent must have a welcome intent to handle this
//                .setChatBotAvatar(getDrawable(R.drawable.avatarBot)) // provide avatar for your bot if default is not required
//                .setChatUserAvatar(getDrawable(R.drawable.avatarUser)) // provide avatar for your the user if default is not required
                .setShowMic(true) // False by Default, True if you want to use Voice input from the user to chat
                .build());

        Bundle bundle = new Bundle();

        // provide a UUID for your session with the Dialogflow agent
        FirebaseUser currentFirebaseUser = FirebaseAuth.getInstance().getCurrentUser() ;
        bundle.putString(ChatbotActivity.SESSION_ID, (currentFirebaseUser != null && !currentFirebaseUser.getUid().equals(""))?currentFirebaseUser.getUid(): UUID.randomUUID().toString());

        Intent intent = new Intent(getActivity(),ChatbotActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_NO_HISTORY);
        intent.putExtras(bundle);
        startActivity(intent);
    }

}
