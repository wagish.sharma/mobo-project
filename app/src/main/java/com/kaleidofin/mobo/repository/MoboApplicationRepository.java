package com.kaleidofin.mobo.repository;

import com.kaleidofin.mobo.model.dto.Goal;
import com.kaleidofin.mobo.model.dto.Goal;
import com.kaleidofin.mobo.model.dto.SpendingPattern;
import com.kaleidofin.mobo.model.dto.SpendingPatternGroup;
import com.kaleidofin.mobo.network.RetrofitServiceFactory;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import java.math.BigDecimal;
import java.util.List;

public class MoboApplicationRepository {

  public Single<SpendingPatternGroup> getDefaultSpendingPatterns(){
    return RetrofitServiceFactory.MOBO_NETWORK_SERVICE.getDefaultSpendingPattern()
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread());
  }

  public Single<SpendingPatternGroup> getOverAllSpendingPatterns(){
    return RetrofitServiceFactory.MOBO_NETWORK_SERVICE.getOverallSpendingPattern()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread());
  }

  public Single<Goal> createGoal(String goalName, BigDecimal goalAmount, Long tenure){
    return RetrofitServiceFactory.MOBO_NETWORK_SERVICE.createGoal(goalName,goalAmount,tenure)
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread());
  }
  public Single<List<Goal>> getCustomerGoals(){
    return RetrofitServiceFactory.MOBO_NETWORK_SERVICE.getCustomerGoals()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread());
  }


}
