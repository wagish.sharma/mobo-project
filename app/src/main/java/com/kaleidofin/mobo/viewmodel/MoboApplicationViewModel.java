package com.kaleidofin.mobo.viewmodel;

import android.app.Application;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.kaleidofin.mobo.NavGraphDirections;
import com.kaleidofin.mobo.R;
import com.kaleidofin.mobo.base.BaseViewModel;
import com.kaleidofin.mobo.base.Resource;
import com.kaleidofin.mobo.model.dto.Goal;
import com.kaleidofin.mobo.model.dto.SpendingPatternGroup;
import com.kaleidofin.mobo.repository.MoboApplicationRepository;
import com.kaleidofin.mobo.utils.DialogButtonActions;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import org.jetbrains.annotations.NotNull;

public class MoboApplicationViewModel extends BaseViewModel {
private final CompositeDisposable disposable = new CompositeDisposable();
private final MutableLiveData<Resource<SpendingPatternGroup>> spendingPatternLiveData = new MutableLiveData<>();
private final MutableLiveData<Resource<Goal>> goalLiveData = new MutableLiveData<>();
private MutableLiveData<Resource<List<Goal>>> goalsLiveData = new MutableLiveData<>();

private MoboApplicationRepository repository;
private String goalName;
private BigDecimal goalAmount;
private Long tenure;
private final MutableLiveData<BigDecimal> contributionAmount = new MutableLiveData<>();
  public MoboApplicationViewModel(@NonNull Application application) {
    super(application);
    repository = new MoboApplicationRepository();
  }

  public LiveData<Resource<SpendingPatternGroup>> getSpendingPatternLiveData() {
    return spendingPatternLiveData;
  }

  public LiveData<Resource<Goal>> getGoalLiveData() {
    return goalLiveData;
  }

  public LiveData<Resource<List<Goal>>> getCustomerGoalsLiveData() {
    return goalsLiveData;
  }

  public void getDefaultSpendingPatterns(){
    disposable.add(repository.getDefaultSpendingPatterns().doOnSubscribe(this::onLoadingOfGetDefaultSpendingPatternsApi)
        .subscribe(this::onSuccessOfGetDefaultSpendingPatternsApi,this::onErrorOfGetDefaultSpendingPatternsApi));
}

  public void getOverallSpendingPatterns(){
    disposable.add(repository.getOverAllSpendingPatterns().doOnSubscribe(this::onLoadingOfGetOverallSpendingPatternsApi)
            .subscribe(this::onSuccessOfGetOverallSpendingPatternsApi,this::onErrorOfGetOverallSpendingPatternsApi));
  }

  public void getCustomerGoals(){
    disposable.add(repository.getCustomerGoals().doOnSubscribe(this::onLoadingOfGetCustomerGoalsApi)
            .subscribe(this::onSuccessOfGetCustomerGoalsApi,this::onErrorOfGetCustomerGoalsApi));
  }

  private void onLoadingOfGetCustomerGoalsApi(Disposable disposable) {
    goalsLiveData.setValue(Resource.loading(null));
  }

  private void onSuccessOfGetCustomerGoalsApi(List<Goal> customerGoals){
    goalsLiveData.setValue(Resource.success(customerGoals));
  }
  private void onErrorOfGetCustomerGoalsApi(Throwable throwable){
    goalsLiveData.setValue(Resource.error(null,null));
    showErrorDialogWithOkButton(throwable.getMessage());
  }

private void onLoadingOfGetDefaultSpendingPatternsApi(Disposable disposable){
    spendingPatternLiveData.setValue(Resource.loading(null));
}

  private void onSuccessOfGetDefaultSpendingPatternsApi(
      SpendingPatternGroup spendingPatternCatagories){
    spendingPatternLiveData.setValue(Resource.success(spendingPatternCatagories));
  }
  private void onErrorOfGetDefaultSpendingPatternsApi(Throwable throwable){
    spendingPatternLiveData.setValue(Resource.error(null,null));
    showErrorDialogWithOkButton(throwable.getMessage());
  }

  private void onErrorOfGetOverallSpendingPatternsApi(Throwable throwable){
    spendingPatternLiveData.setValue(Resource.error(null,null));
    showErrorDialogWithOkButton(throwable.getMessage());
  }

  private void onLoadingOfGetOverallSpendingPatternsApi(Disposable disposable){
    spendingPatternLiveData.setValue(Resource.loading(null));
  }

  private void onSuccessOfGetOverallSpendingPatternsApi(SpendingPatternGroup spendingPatternCatagories){
    spendingPatternLiveData.setValue(Resource.success(spendingPatternCatagories));
  }


  private void showErrorDialogWithOkButton(String message ) {
    DialogButtonActions buttonActions = new DialogButtonActions();
    buttonActions
        .setPositiveAction((dialog, position) -> popBackStack(R.id.errorDialogFragment, true));
    navigate(NavGraphDirections.actionErrorDialog()
        .setDialogTitle("Error")
        .setDialogMessage(message)
        .setPositionButtonText("OK")
        .setButtonActions(buttonActions));
  }


  public void navigateAndCalculateContributionAmount(String goalName,long goalAmount, int tenure){
    this.goalName = goalName;
    this.goalAmount = BigDecimal.valueOf(goalAmount);
    this.tenure = (long) tenure;
    contributionAmount.setValue(calculateContributionAmount(BigDecimal.valueOf(goalAmount),BigDecimal.valueOf(tenure)));
    navigate(R.id.confirmGoalFragment);
  }


  private BigDecimal calculateContributionAmount(@NotNull BigDecimal goalAmount, @NotNull BigDecimal duration){
    BigDecimal intervalRate = BigDecimal.valueOf(0.045).divide(new BigDecimal(12), 20, BigDecimal.ROUND_HALF_UP);
    BigDecimal intervalRatePlusOne = intervalRate.add(BigDecimal.ONE);
    BigDecimal multiplierNumerator = new BigDecimal(Math.pow(intervalRatePlusOne.doubleValue(), duration.doubleValue())).setScale(20, RoundingMode.HALF_UP);
    BigDecimal multiplierDenominator = multiplierNumerator.subtract(BigDecimal.ONE);
    //to handle divide by zero error
    if(multiplierDenominator.compareTo(BigDecimal.ZERO) == 0){
      return BigDecimal.ZERO;
    }
    BigDecimal contributionAmount = (goalAmount.multiply(intervalRate)).divide(multiplierDenominator, 0, RoundingMode.HALF_UP);

    return contributionAmount;
  }

  public void createGoal(){
    disposable.add(repository.createGoal(goalName,goalAmount,tenure).doOnSubscribe(this::onLoadingOfCreateGoalsApi)
        .subscribe(this::onSuccessOfCreateGoalsApi,this::onErrorOfCreateGoalsApi));
  }


  private void onErrorOfCreateGoalsApi(Throwable throwable){
    goalLiveData.setValue(Resource.error(null,null));
    showErrorDialogWithOkButton(throwable.getMessage());
  }

  private void onLoadingOfCreateGoalsApi(Disposable disposable){
    goalLiveData.setValue(Resource.loading(null));
  }

  private void onSuccessOfCreateGoalsApi(Goal goal){
    goalLiveData.setValue(Resource.success(goal));
    navigate(R.id.dashboardFragment);
  }

  @Override
  protected void onCleared() {
    disposable.dispose();
    super.onCleared();
  }

  public LiveData<BigDecimal> getContributionAmount() {
    return contributionAmount;
  }
}
