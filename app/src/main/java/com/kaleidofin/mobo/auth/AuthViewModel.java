package com.kaleidofin.mobo.auth;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;

public class AuthViewModel extends AndroidViewModel {
    private static final String TAG = "RegisterViewModel";

    public AuthViewModel(@NonNull Application application) {
        super(application);
    }
}
