package com.kaleidofin.mobo.base;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import androidx.annotation.CallSuper;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import io.reactivex.disposables.CompositeDisposable;

public abstract class BaseFragment<T extends BaseViewModel> extends Fragment {

    private static final int NO_HOST_FRAGMENT = -1;
    private T viewModel;
    private AppCompatActivity context;
    private int navHostFragmentId;
    private CompositeDisposable disposable = new CompositeDisposable();
    private NavController navController;
    private AppBarConfiguration appBarConfiguration;

    @CallSuper
    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        try {
            this.context = (AppCompatActivity) context;
        } catch (ClassCastException exception) {
            throw new IllegalArgumentException("The parent Activity must extend AppCompatActivity");
        }
    }

    @CallSuper
    @Override
    public void onDetach() {
        super.onDetach();
        context = null;
    }

    public abstract T getViewModel();

    public abstract int getNavHostFragmentId();

    @CallSuper
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModel = getViewModel();
        navHostFragmentId = getNavHostFragmentId();
    }

    @CallSuper
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        navController = Navigation.findNavController(context, navHostFragmentId);
    }

    public void setUpToolBarWithNavigationUI(Toolbar toolbar) {
        appBarConfiguration =
            new AppBarConfiguration.Builder(navController.getGraph()).build();
        NavigationUI.setupWithNavController(toolbar, navController, appBarConfiguration);
    }

    @CallSuper
    @Override
    public void onStart() {
        super.onStart();
        if (viewModel != null & navHostFragmentId != NO_HOST_FRAGMENT) {
            disposable.add(viewModel.getNavigationCommand()
                .subscribe(navigationCommand -> {
                    if (navigationCommand instanceof NavigationCommand.To) {

                        navController.navigate(((NavigationCommand.To) navigationCommand).getDirections());

                    } else if (navigationCommand instanceof NavigationCommand.BackTo) {

                        navController
                            .navigate(((NavigationCommand.BackTo) navigationCommand).getDestination());

                    } else if (navigationCommand instanceof NavigationCommand.Back) {

                        context.onBackPressed();

                    } else if (navigationCommand instanceof NavigationCommand.PopBackStack) {

                        NavigationCommand.PopBackStack popBackStack = (NavigationCommand.PopBackStack) navigationCommand;
                        navController.popBackStack(popBackStack.getDestination(), popBackStack.isInclusive());

                    } else if (navigationCommand instanceof NavigationCommand.NavigateWithBundle) {

                        NavigationCommand.NavigateWithBundle navigateWithBundle = (NavigationCommand.NavigateWithBundle) navigationCommand;
                        navController
                            .navigate(navigateWithBundle.getDestination(), navigateWithBundle.getBundle());
                    }
                }));

        }

    }

    @CallSuper
    @Override
    public void onStop() {
        disposable.clear();
        super.onStop();
    }
}