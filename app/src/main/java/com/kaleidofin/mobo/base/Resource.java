package com.kaleidofin.mobo.base;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class Resource<VIEWSTATE> {

  @NonNull
  public final Status status;
  @Nullable
  public final VIEWSTATE viewState;
  @Nullable
  public final String message;

  public Resource(@NonNull Status status, @Nullable VIEWSTATE viewState,
      @Nullable String message) {
    this.status = status;
    this.viewState = viewState;
    this.message = message;
  }

  public static <VIEWSTATE> Resource<VIEWSTATE> success(@NonNull VIEWSTATE data) {
    return new Resource<>(Status.SUCCESS, data, null);
  }

  public static <VIEWSTATE> Resource<VIEWSTATE> error(String msg, @Nullable VIEWSTATE data) {
    return new Resource<>(Status.ERROR, data, msg);
  }

  public static <VIEWSTATE> Resource<VIEWSTATE> loading(@Nullable VIEWSTATE data) {
    return new Resource<>(Status.LOADING, data, null);
  }

  public enum Status {SUCCESS, ERROR, LOADING}
}